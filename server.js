const { ApolloServer, gql} = require('apollo-server');
const { Neo4jGraphQL } = require('@neo4j/graphql');
const neo4j = require('neo4j-driver');
require("dotenv").config();

/** Define types for the different entities */
const typeDefs = gql`
type Test{
    id: ID! @id
    name: String!
    questions: [Question!]! @relationship(type:"HAS" direction: OUT) 
}

type Question{
    id: ID! @id
    name: String!
    answer: String!
    category: Category! @relationship(type:"BELONGS_TO", direction: OUT)

}

type Category{
    id: ID @id
    name: String!
    description: String
}
`;

const driver = neo4j.driver(process.env.NEO4J_URI, neo4j.auth.basic(process.env.NEO4J_USER, process.env.NEO4J_PASSWORD));

const neoSchema = new Neo4jGraphQL({typeDefs, driver});

neoSchema.getSchema().then((schema)=>{
    const server = new ApolloServer({
        schema: schema
    });

    server.listen().then(({ url }) => {
        console.log(`🚀 Server ready at ${url}`);
    });
})

