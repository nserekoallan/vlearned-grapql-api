const neo4j = require("neo4j-driver");
const uri = "bolt://localhost:7474";
const user = "neo4j";
const password = "1234";

export default class Database {
     async createConnection(){
        const options = {
            maxConnectionLifetime: 3 * 60 * 60 * 1000, // 3 hours
            maxConnectionPoolSize: 50,
            connectionAcquisitionTimeout: 2 * 60 * 1000, // 120 seconds
            disableLosslessIntegers: true
        };

        return neo4j.driver(uri, neo4j.auth.basic(user, password), options, function (err, client) {
            if (err) {
                throw new InternalError(err.message);
            }

            state.db = client;
            state.link = uri;
            state.client = client;

            return client;
        });
    }

    static async getConnection(){
        if (state.db) {
            return state.db;
        } else {
            return await this.createConnection();
        }
    }

    static async closePoolAndExit() {
        if (state.client) {
            state.client.close().then(() => {
                state.client = null;
                state.db = null;
            });
        }
    }
}
