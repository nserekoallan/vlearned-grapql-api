const { gql } = require('apollo-server');

module.exports = gql`
type Query {
    tests: [Test]
    test(name: String!): Test 
}

type Mutation {
    createTest(input: createTestInput): Test
    deleteTest(name: String!): Boolean
}

input createTestInput{
    name: String!
    questions: [String]
}

type Test{
    name: String!
    questions: [Question!]! @relationship(type:"HAS" direction: OUT) 
}

`