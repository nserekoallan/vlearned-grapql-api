const { gql } = require('apollo-server');

module.exports = gql`
type Query{
    lessons: [Lesson]
    lesson(id:String): Lesson
    }

type Mutation{
    createLesson(name:String, level: String, description:String, link:String, topic:String): Lesson
}

type Lesson{
    name: String
    description: String
    level: String
    topic: String
    tests:[Test!]! @relationship(type:"HAS", direction: OUT)
}
`;