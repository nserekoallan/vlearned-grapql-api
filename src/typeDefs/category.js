const { gql } = require("apollo-server");

module.exports = gql`
type Query{
    categories: [Category]
    category(id: Int): Category
}

type Mutation{
    createCategory(input: createCategoryInput): Category
    deleteCategory(id: Int): Boolean
}

type Category{
    name: String!
    description: String
}

input createCategoryInput{
    name: String!,
    description: String
}
`